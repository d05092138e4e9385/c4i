<?php
    header('Content-type: text/html; charset=utf-8');
?>
<?php include $_SERVER['DOCUMENT_ROOT'].'/blocks/takeLanguage.php'; ?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name='viewport' content='width=device-width,initial-scale=1,maximum-scale=1'/>
    <title>C4I</title>
    <link href="/styles/main.min.css" rel="stylesheet">
    <link href="/styles/style.min.css" rel="stylesheet">
</head>
<body>
<div class="b-top">
    <?php include $_SERVER['DOCUMENT_ROOT'].'/blocks/menu-top/index.php'; ?>
</div>
<div class="b-center">
    <?php include $_SERVER['DOCUMENT_ROOT'].'/blocks/content/index.php'; ?>
</div>
<div class="b-bottom">
    <?php include $_SERVER['DOCUMENT_ROOT'].'/blocks/bottom/index.php'; ?>
</div>
</body>
</html>