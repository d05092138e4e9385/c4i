let imageURLBlob;
let imgName;
let fileData = {};

if( document.readyState !== 'loading' ) {
    init();
} else {
    document.addEventListener("DOMContentLoaded", init);
}

function init(){
    initRadiobutton();
    document.querySelector('.en-cen-txt-or-fl-txt').addEventListener('click', showEncodeInputDataText);
    document.querySelector('.en-cen-txt-or-fl-fl').addEventListener('click', showEncodeInputDataFile);
    document.querySelector('.en-cen-input > textarea').addEventListener('input', function(){
        showImageSizeNeeded(document.querySelector('.en-cen-input > textarea').value);
    });
    document.querySelector('.en-cen-txt-or-fl-txt').addEventListener('click', function(){
        showImageSizeNeeded(document.querySelector('.en-cen-input > textarea').value);
    });
    document.querySelector('.en-cen-txt-or-fl-fl').addEventListener('click', function(){
        showImageSizeNeeded(fileData.size || '');
    });

    document.querySelector('.encod-right__input > textarea').addEventListener('input', checkingCloseButton);
    document.querySelector('.decoding-output-data-text-copy-button').addEventListener('click', copyTextDecoding);
    document.querySelector('.de-password-input > textarea').addEventListener('input', function(){
        showHideEasyBlueButton('show', document.querySelector('.decoding'), 'decoding');
    });
    showEncodingMenu();
    document.querySelector('.js-button-encoding').classList.add('radiobutton-active');
    //document.querySelector('.content-container').style.display = 'flex';
    document.querySelector('.decoding-download').addEventListener('click', function(){
        document.querySelector('.decoding-download-link').click();
    });
    //window.addEventListener('resize', checkSizeForCompliance);
}

function checkSizeForCompliance() {
    let width = document.documentElement.clientWidth;

    console.log(width);
}

function radiobutton(event){
    let elem = event.target;
    let elems = elem.parentElement.querySelectorAll('.radiobutton');

    for(let i = 0; i < elems.length; i++){
        if(elems[i].classList.contains('radiobutton-active')) {
            elems[i].classList.remove('radiobutton-active');
        }
    }
    if(elem.dataset.radioButtonFunctionName){
        window[elem.dataset.radioButtonFunctionName]();
    }
    elem.classList.add('radiobutton-active');
}
function initRadiobutton(){
    let elems = document.querySelectorAll('.radiobutton');
    for(let i = 0; i < elems.length; i++){
        elems[i].addEventListener('click', radiobutton);
    }
}


// GRAPHIC

function showEncodingMenu(){
    if(document.querySelector('.js-button-encoding').classList.contains('radiobutton-active')) {
        return;
    }
    document.querySelector('.img-info__available > .img-info-x-size').innerHTML = '— x —';
    document.querySelector('.img-info__available > .img-info-full-size').innerHTML = '—';
    document.querySelector('.en-cen-input > textarea').value = '';
    document.querySelector('.encod-right__input > textarea').value = '';
    document.querySelector('.encoding-container').style.display = 'flex';
    document.querySelector('.decoding-container').style.display = 'none';
    document.querySelector('.img-info__available').dataset.imageSize = '0';
    fileData = {};
    document.querySelector('.dropZone-en-im > .dz-fileName > div').innerText = '';
    document.querySelector('.dropZone-en-f > .dz-fileName > div').innerText = '';
    showHideEasyBlueButton('hide', document.querySelector('.encoding-download'), 'clickEncodeDownloadLink');
    showImageSizeNeeded('');
}
function showDecodingMenu(){
    if(document.querySelector('.js-button-decoding').classList.contains('radiobutton-active')) {
        return;
    }
    document.querySelector('.de-password-input > textarea').value = '';
    document.querySelector('.decoding-output-data__file').style.display = 'none';
    document.querySelector('.decoding-output-data__text').style.display = 'none';
    document.querySelector('.decoding-output-data__null').style.display = 'none';
    showHideEasyBlueButton('hide', document.querySelector('.decoding'), 'decoding');
    fileData = {};
    document.querySelector('.dropZone-de-im > .dz-fileName > div').innerText = '';
    document.querySelector('.encoding-container').style.display = 'none';
    document.querySelector('.decoding-container').style.display = 'flex';

}

function checkingCloseButton(){
    let elemAvailable = document.querySelector('.img-info__available');
    let elemNeed = document.querySelector('.img-info__need');

    if(((document.querySelector('.en-cen-input>textarea').value !== '' && document.querySelector('.en-cen-input').style.display !== 'none')
        || (fileData.byteData !== undefined && document.querySelector('.dropZone-en-f').style.display !== 'none'))
        && imageURLBlob !== undefined){
        if(+elemAvailable.dataset.imageSize > +elemNeed.dataset.imageSize){
            showHideEasyBlueButton('show', document.querySelector('.encoding'), 'encoding');
            if(!elemAvailable.classList.contains('img-info-green')) {
                elemAvailable.classList.add('img-info-green');
                elemAvailable.classList.remove('img-info-red');
            }
            return;
        }
        if(!elemAvailable.classList.contains('img-info-red')){
            elemAvailable.classList.remove('img-info-green');
            elemAvailable.classList.add('img-info-red');
        }
    } else{
        elemAvailable.classList.remove('img-info-green');
        elemAvailable.classList.remove('img-info-red');
    }
    showHideEasyBlueButton('hide', document.querySelector('.encoding'), 'encoding');
}
function checkingForTheDataEntered(){
    let output = true;
    if(document.querySelector('.en-cen-input>textarea').value === '' && fileData.byteData === undefined){
        //createErrInfo('#choiceDataType', phrase.errInfoAddData[lang]);
        output = false;
    }
    if(imageURLBlob === undefined) {
        //createErrInfo('#edz', phrase.errInfoAddImg[lang]);
        output = false;
    }
    return output;
}

function showEncodeInputDataText(){
    document.querySelector('.en-cen-input').style.display = 'flex';
    document.querySelector('.dropZone-en-f').style.display = 'none';
    showImageSizeNeeded();
}
function showEncodeInputDataFile(){
    document.querySelector('.dropZone-en-f > .dz-fileName > div').innerText = '';
    document.querySelector('.en-cen-input').style.display = 'none';
    document.querySelector('.dropZone-en-f').style.display = 'flex';
    showImageSizeNeeded();
}

function showHideEasyBlueButton(strShowOrHide, element, functionName){
    if(strShowOrHide === 'show'){
        if(element.classList.contains('button-gray')){
            element.classList.add('button-easy-blue');
            element.classList.remove('button-gray');

            if(functionName === 'encoding') {
                element.addEventListener('click', encoding);
            }
            else if(functionName === 'clickEncodeDownloadLink') {
                element.addEventListener('click', clickEncodeDownloadLink);
            }
            else if(functionName === 'decoding') {
                element.addEventListener('click', decoding);
            }
        }
    } else if(strShowOrHide === 'hide'){
        if(element.classList.contains('button-easy-blue')){
            element.classList.add('button-gray');
            element.classList.remove('button-easy-blue');

            if(functionName === 'encoding') {
                element.removeEventListener('click', encoding);
            }
            else if(functionName === 'clickEncodeDownloadLink') {
                element.removeEventListener('click', clickEncodeDownloadLink);
            }
            else if(functionName === 'decoding') {
                element.removeEventListener('click', decoding);
            }
        }
    }
}

function showImageSize(img){
    imgW = img.width;
    imgH = img.height;

    document.querySelector('.img-info__available > .img-info-x-size').innerHTML =
        addSpacesAfterThreeCharacters(img.width) + ' x ' + addSpacesAfterThreeCharacters(img.height);
    document.querySelector('.img-info__available > .img-info-full-size').innerHTML = addSpacesAfterThreeCharacters(imgW * imgH);
    document.querySelector('.img-info__available').dataset.imageSize = (imgW * imgH) + '';



    drawImageCanvas(img);
    checkingCloseButton();
}
function drawImageCanvas(img){
    document.querySelector('.divCanvas').innerHTML =
        '<canvas id="canvas_picker" width="' + img.width + '" height="' + img.height + '"></canvas>';

    let canvas = document.getElementById('canvas_picker').getContext('2d');
    canvas.drawImage(img,0,0);
}
function showImageSizeNeeded(data){
    let requiredSizeFull = 0;
    let technicalData;
    let Ssqrt;

    if(typeof(data) == 'string') {
        requiredSizeFull = Math.ceil((calcByteLength(data) * 8) / 3.0);
    } else if(typeof(data) == 'number'){
        requiredSizeFull = Math.ceil(((data + fileData.name.length) * 8) / 3.0);
    } else {
        return false;
    }

    technicalData = decToBin(requiredSizeFull) + '00';
    while(technicalData.length % 3 !== 0)				// err max size
        technicalData += '0';
    technicalData = (technicalData.length / 3) + 2;
    requiredSizeFull +=  technicalData;
    Ssqrt = Math.ceil(Math.sqrt(requiredSizeFull));

    document.querySelector('.img-info__need > .img-info-x-size').innerText =
        addSpacesAfterThreeCharacters(Ssqrt) + ' x ' + addSpacesAfterThreeCharacters(Ssqrt);
    document.querySelector('.img-info__need > .img-info-full-size').innerText =
        addSpacesAfterThreeCharacters(requiredSizeFull);
    document.querySelector('.img-info__need').dataset.imageSize = requiredSizeFull +'';
    checkingCloseButton();
}

function showDecodingNullData(){
    document.querySelector('.decoding-output-data__file').style.display = 'none';
    document.querySelector('.decoding-output-data__text').style.display = 'none';
    document.querySelector('.decoding-output-data__null').style.display = 'flex';
}

function addSpacesAfterThreeCharacters(number){
    number += '';

    for(let i = number.length - 3; i > 0; i -= 3){
        number = number.substr(0, i) + ' ' + number.substr(i);
    }

    return number;
}
function calcByteLength(str) {
    let m = encodeURIComponent(str).match(/%[89ABab]/g);
    return str.length + (m ? m.length : 0);
}

function clickEncodeDownloadLink(){
    document.querySelector('.encoding-download-link').click();
}

function copyTextDecoding(){
    let elem = document.querySelector('.decoding-output-data-text-block > textarea');
    elem.select();
    document.execCommand("copy");
}

// FILE
function openFile(file, elem){
    let reader = new FileReader();
    reader.onload = function(event) {

        if(elem.classList.contains('dropZone-en-im') || elem.classList.contains('dropZone-de-im')){
            imageURLBlob = URL.createObjectURL(new Blob([event.target.result]));

            let testImg = new Image();
            testImg.onload = function(){
                if(elem.classList.contains('dropZone-en-im')) {
                    showImageSize(testImg);
                    document.querySelector('.dropZone-en-im > .dz-fileName > div').innerText = file[0].name.replace(/\n/g, ' ');
                }
                else {
                    showHideEasyBlueButton('show', document.querySelector('.decoding'), 'decoding');
                    drawImageCanvas(testImg);
                    document.querySelector('.dropZone-de-im > .dz-fileName > div').innerText = file[0].name.replace(/\n/g, ' ');
                }
            };
            testImg.src = imageURLBlob;
        } else {
            fileData.name = '' + file[0].name.replace(/\n/g, ' ');
            fileData.type = '' + file[0].type;
            fileData.byteData = new Uint8Array(event.target.result);
            fileData.size = file[0].size;

            if(elem.classList.contains('dropZone-en-f')){
                showImageSizeNeeded(fileData.size);
                document.querySelector('.dropZone-en-f > .dz-fileName > div').innerText = file[0].name.replace(/\n/g, ' ');
            }
        }

    };
    reader.readAsArrayBuffer(file[0]);

    if(elem.classList.contains('dropZone-en-im') || elem.classList.contains('dropZone-de-im')){
        imgName = file[0].name;
    }
}

// ENCODING
function encoding(){
    if(checkingForTheDataEntered() === false) {
        return;
    }
    setTimeout(function(){
        showHideEasyBlueButton('hide', document.querySelector('.encoding'), 'encoding');
        showHideEasyBlueButton('hide', document.querySelector('.encoding-download'), 'clickEncodeDownloadLink');
    },0);

    let tInterval = setInterval(function(){
        if(document.querySelector('.encoding-download').classList.contains('button-easy-blue')) {
            return;
        }
        clearInterval(tInterval);
        setTimeout(function(){
            let inputData;
            let dataStr = '';
            let binArr;
            let requiredSize_full;
            let technicalData;
            let decode;
            let textOrFile;
            let uintFileName;
            let technicalDataSize;
            let img = document.querySelector('#canvas_picker');

            if(document.querySelector('.en-cen-input').style.display === 'none'){	//file
                console.log('file');
                textOrFile = '1';
                uintFileName = stringToUint8Array(fileData.name + '\n\n');
                inputData =   combineUnit8Arrays(uintFileName, fileData.byteData);
            } else {                                                                        // text
                textOrFile = '0';
                inputData = toUTF8Array(document.querySelector('.en-cen-input > textarea').value);
                if (inputData === '') {
                    return;
                }
            }
            binArr = decToBin(inputData);

            for(let i = 0; i < binArr.length; i++){
                while(binArr[i].length % 8 !== 0)
                    binArr[i] = '0' + binArr[i];
                dataStr += binArr[i];
            }

            requiredSize_full = Math.ceil(dataStr.length / 3.0);
            technicalData = decToBin(requiredSize_full);
            decode = '0';
            technicalData = technicalData + decode + textOrFile;
            while(technicalData.length % 3 !== 0)
                technicalData = '0' + technicalData;

            technicalDataSize = decToBin(Math.ceil(technicalData.length / 3.0)) + '';		// err max size
            if(technicalDataSize.length > 6) {return;}
            while(technicalDataSize.length < 6)
                technicalDataSize = '0' + technicalDataSize;

            //while(dataStr.length % 3 !== 0)
            //    dataStr = '0' + dataStr;

            dataStr = technicalDataSize + technicalData + dataStr;

            if((dataStr.length / 3) > img.width * img.height){
                //createErrInfo('#textInputArea', phrase.errInfoImgSmall[lang] + '.');
                console.log('image too small');
                return;
            }
            writeToImage(dataStr);
        },10);
    },10);

}

function stringToUint8Array(string){
    let charCodeArray = new Uint8Array(string.length);
    for (let i = 0, strLength = string.length; i < strLength; i++) {
        charCodeArray[i] = string.charCodeAt(i);
    }
    return charCodeArray;
}
function combineUnit8Arrays(arr1, arr2){
    let buf = new ArrayBuffer(arr1.length + arr2.length);
    let byteArray = new Uint8Array(buf);
    let i, ii;

    i = ii = 0;
    for (let bufLength = arr1.length; i < bufLength; i++) {
        byteArray[i] = arr1[i];
    }
    for (let bufLength = arr2.length; ii < bufLength; ii++) {
        byteArray[i + ii] = arr2[ii];
    }
    return byteArray;
}

function toUTF8Array(str) {
    let utf8 = [];
    for (let i = 0; i < str.length; i++) {
        let charcode = str.charCodeAt(i);
        if (charcode < 0x80) {utf8.push(charcode);}
        else if (charcode < 0x800) {
            utf8.push(0xc0 | (charcode >> 6),
                0x80 | (charcode & 0x3f));
        }
        else if (charcode < 0xd800 || charcode >= 0xe000) {
            utf8.push(0xe0 | (charcode >> 12),
                0x80 | ((charcode>>6) & 0x3f),
                0x80 | (charcode & 0x3f));
        }
        // surrogate pair
        else {
            i++;
            // UTF-16 encodes 0x10000-0x10FFFF by
            // subtracting 0x10000 and splitting the
            // 20 bits of 0x0-0xFFFFF into two halves
            charcode = 0x10000 + (((charcode & 0x3ff)<<10)
                | (str.charCodeAt(i) & 0x3ff));
            utf8.push(0xf0 | (charcode >>18),
                0x80 | ((charcode>>12) & 0x3f),
                0x80 | ((charcode>>6) & 0x3f),
                0x80 | (charcode & 0x3f));
        }
    }
    return utf8;
}
function decToBin(dec){
    let output;
    if(typeof(dec) == "object"){
        output = [];
        for(let i = 0; i < dec.length; i++)
            output.push((+dec[i]).toString(2));
    } else {
        output = (+dec).toString(2);
    }
    return output;
}
function shuffleFisherYates(array, passwordString) {
    let currentIndex = array.length, temporaryValue, randomIndex;
    let prng = new Math.seedrandom(passwordString);
    // While there remain elements to shuffle...
    while (currentIndex !== 0) {
        // Pick a remaining element...
        randomIndex = Math.floor((prng(currentIndex)) * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}

function writeToImage(str){
    let x, y;
    let i;
    let xMax, yMax;
    let imgSize;
    //let requiredSize_full;
    let canvas;
    let imgData;
    let r, g, b;
    let blobUrl;
    //let minimumProgressValue; //, progressValue;
    let arr = [];
    let passwordString;

    xMax = imgW;
    yMax = imgH;
    imgSize = xMax * yMax;

    //requiredSize_full = minImageSize(document.querySelector('#textInputArea').value);
    canvas = document.getElementById('canvas_picker').getContext('2d');

    //imgData = canvas.getImageData(0, 0, xMax, Math.ceil((requiredSize_full + 0.0) / xMax)).data;
    imgData = canvas.getImageData(0, 0, xMax, yMax).data;
    //minimumProgressValue = Math.ceil(100 / str.length);

    for(i = 0; i < imgSize; i++)
        arr.push(i);

    passwordString = document.querySelector('.encod-right__input > textarea').value;
    if(passwordString !== '') {
        arr = shuffleFisherYates(arr, passwordString);
    }

    for(i = 0; i < imgSize && (i*3) < str.length; i++){
        r = imgData[(arr[i] * 4)];
        g = imgData[(arr[i] * 4) + 1];
        b = imgData[(arr[i] * 4) + 2];

        if(r === 255) {r = 253;}
        if(g === 255) {g = 253;}
        if(b === 255) {b = 253;}

        // red
        if(str[((i*3)+3)-3] === '0') {
            r = (r + ((r % 2 === 0) ? 0 : 1));
        } else {
            r = (r + ((r % 2 === 0) ? 1 : 0));
        }
        // green
        if(str[((i*3)+3)-2] === '0') {
            g = (g + ((g % 2 === 0) ? 0 : 1));
        } else {
            g = (g + ((g % 2 === 0) ? 1 : 0));
        }
        // blue
        if(str[((i*3)+3)-1] === '0') {
            b = (b + ((b % 2 === 0) ? 0 : 1));
        } else {
            b = (b + ((b % 2 === 0) ? 1 : 0));
        }

        x = arr[i] % xMax;
        y = Math.trunc(arr[i] / xMax);

        canvas.fillStyle = 'rgb(' + r + ',' + g + ',' + b +')';
        canvas.fillRect(x, y, 1, 1);
    }
    //outputLog('Encoding complete');

    document.getElementById('canvas_picker').toBlob(function(blob){
        blobUrl=URL.createObjectURL(blob, blob.type);
        createImageAfterEncoding(blobUrl)
    },'image/png');
}
function createImageAfterEncoding(url){
    let newName;
    let l = imgName.lastIndexOf('.');
    if(l === -1) {
        newName = imgName + '_encode';
    } else {
        newName = imgName.substr(0, l) + '_encode.png';
    }

    document.querySelector('.encoding-download-link').href = url;
    document.querySelector('.encoding-download-link').download = newName;
    showHideEasyBlueButton('show', document.querySelector('.encoding-download'), 'clickEncodeDownloadLink');
    //showHideEasyBlueButton('show', document.querySelector('.encoding'), 'encoding');
}

// DECODING
function decoding(){
    setTimeout(function(){
        showHideEasyBlueButton('hide', document.querySelector('.decoding'), 'decoding');
    },0);
    if(imageURLBlob === ''){
        //createErrInfo('#ddz', 2 'insert the image');
        return;
    }
    //document.querySelector('#consoleLogLabel').value = '';
    let i;

    let technicalDataSize = readFromImage(0, 2);
    if(technicalDataSize === false || technicalDataSize.length === 0){
        console.log('false1');
        showDecodingNullData();
        return;
    }

    let technicalData = readFromImage(2, parseInt(technicalDataSize, 2));
    if(technicalData === false || technicalData.length === 0){
        console.log('false2');
        showDecodingNullData();
        return;
    }

    //let zip = (technicalData[technicalData.length - 2]);
    let textOrFile = (technicalData[technicalData.length - 1]);
    let dataStr = readFromImage((2 + Math.ceil(technicalData.length / 3.0)),
        parseInt(technicalData.substr(0, [technicalData.length - 2]), 2));
    if(dataStr === false || dataStr.length === 0){
        console.log('false3');
        showDecodingNullData();
        return;
    }

    let binArr = [];
    for(i = 0; (dataStr.length - i) % 8 !== 0; i++);
    dataStr = dataStr.substr(0, dataStr.length - i);

    for(i = 0; (dataStr.length - i) > 0; i += 8){
        binArr.push(parseInt(dataStr.substr(i, 8), 2));
    }

    if(textOrFile === '1'){
        let l;
        let fileName = '';
        let file;
        let reader;

        for(let i = 0; i < binArr.length -1; i++){
            if(binArr[i] === 10){
                if(binArr[i+1] === 10) {
                    l = i;
                    break;
                }
                //else					i++;
            }
        }
        for(let i = 0; i < l; i++){
            fileName += String.fromCharCode(binArr[i]);
        }

        for(let i = 0; i < l +2; i++){
            binArr.shift();
        }

        file = new File([charArrayToArrayBuffer(binArr)], 'fileName');
        reader = new FileReader();

        reader.onload = function(e) {
            let blob = new Blob([e.target.result]);
            document.querySelector('.decoding-download-link').href = URL.createObjectURL(blob);
            document.querySelector('.decoding-download-link').download = fileName;
            document.querySelector('.decoding-output-data-file-name').innerText = fileName;
            document.querySelector('.decoding-output-data__text').style.display = 'none';
            document.querySelector('.decoding-output-data__null').style.display = 'none';
            document.querySelector('.decoding-output-data__file').style.display = 'flex';
        };
        reader.readAsArrayBuffer(file);
    } else {
        document.querySelector('.decoding-output-data-text-block > textarea').value = fromUTF8Array(binArr);
        document.querySelector('.decoding-output-data__file').style.display = 'none';
        document.querySelector('.decoding-output-data__null').style.display = 'none';
        document.querySelector('.decoding-output-data__text').style.display = 'flex';
    }
}

function readFromImage(startPixel, count){
    let arr = [];
    let xMax, yMax;
    let y;
    let canvas;
    let r, g, b;
    let bytes = '';
    let imgData;
    let passwordString;
    let imgSize;
    let img = document.querySelector('#canvas_picker');

    xMax = img.width;
    yMax = img.height;
    imgSize = img.width * img.height;
    if(count > imgSize) {return false;}

    canvas = document.getElementById('canvas_picker').getContext('2d');
    y = Math.trunc(startPixel / xMax);
    for(let i = 0; i < imgSize; i++) arr.push(i);
    passwordString = document.querySelector('.de-password-input > textarea').value;

    if(passwordString !== ''){
        arr = shuffleFisherYates(arr, passwordString);
        imgData = canvas.getImageData(0, 0, xMax, yMax+1).data;
    } else {
        imgData = canvas.getImageData(0, y, xMax, (Math.ceil((count + 0.0) / xMax)+1)).data;
        startPixel = startPixel % xMax;
    }

    for(let i = 0; i < count; i++){
        r = imgData[(arr[startPixel + i] * 4)];
        g = imgData[(arr[startPixel + i] * 4) + 1];
        b = imgData[(arr[startPixel + i] * 4) + 2];
        bytes = bytes + ((r % 2  === 0) ? "0" : "1");
        bytes = bytes + ((g % 2  === 0) ? "0" : "1");
        bytes = bytes + ((b % 2  === 0) ? "0" : "1");
    }

    return bytes;
}
function fromUTF8Array(arr) { // array of bytes to string
    let str = '',
        i;

    for (i = 0; i < arr.length; i++) {
        let value = arr[i];

        if (value < 0x80) {
            str += String.fromCharCode(value);
        } else if (value > 0xBF && value < 0xE0) {
            str += String.fromCharCode((value & 0x1F) << 6 | arr[i + 1] & 0x3F);
            i += 1;
        } else if (value > 0xDF && value < 0xF0) {
            str += String.fromCharCode((value & 0x0F) << 12 | (arr[i + 1] & 0x3F) << 6 | arr[i + 2] & 0x3F);
            i += 2;
        } else {
            // surrogate pair
            let charCode = ((value & 0x07) << 18 | (arr[i + 1] & 0x3F) << 12 | (arr[i + 2] & 0x3F) << 6 | arr[i + 3] & 0x3F) - 0x010000;

            str += String.fromCharCode(charCode >> 10 | 0xD800, charCode & 0x03FF | 0xDC00);
            i += 3;
        }
    }
    return str;
}
function charArrayToArrayBuffer(charArray){
    let buf = new ArrayBuffer(charArray.length);
    let byteArray = new Uint8Array(buf);
    for (let i = 0, bufLength = charArray.length; i < bufLength; i++) {
        byteArray[i] = charArray[i];
    }
    return byteArray;
}