<?php
$phrase	= array(
    "encoding" => array(
        "en" => "Encoding",
        "ru" => "Кодирование",
        "uk" => "Кодування"
    ),
    "encode" => array(
        "en" => "Encode",
        "ru" => "Кодировать",
        "uk" => "Кодувати"
    ),
    "decoding" => array(
        "en" => "Decoding",
        "ru" => "Декодирование",
        "uk" => "Декодування"
    ),
    "decode" => array(
        "en" => "Decode",
        "ru" => "Декодировать",
        "uk" => "Декодувати"
    ),
    "image" => array(
        "en" => "Image",
        "ru" => "Изображение",
        "uk" => "Зображення"
    ),
    "dropImage" => array(
        "en" => "drop image here",
        "ru" => "Перенесите изображение сюда",
        "uk" => "Перенесіть зображення сюди"
    ),
    "pixels" => array(
        "en" => "pixels",
        "ru" => "пикселей",
        "uk" => "пікселів"
    ),
    "size" => array(
        "en" => "size",
        "ru" => "размер",
        "uk" => "розмір"
    ),
    "need" => array(
        "en" => "need",
        "ru" => "необходимо",
        "uk" => "необхідно"
    ),
    "inStock" => array(
        "en" => "in stock",
        "ru" => "в наличии",
        "uk" => "наявно"
    ),
    "data" => array(
        "en" => "Data for hiding ",
        "ru" => "Данные для сокрытия",
        "uk" => "Дані для приховування"
    ),
    "text" => array(
        "en" => "text",
        "ru" => "текст",
        "uk" => "текст"
    ),
    "file" => array(
        "en" => "file",
        "ru" => "файл",
        "uk" => "файл"
    ),
    "txtForEnc" => array(
        "en" => "Input text for encoding",
        "ru" => "Введите текст для кодирования",
        "uk" => "Введіть текст для кодування"
    ),
    "dropFile" => array(
        "en" => "drop file here",
        "ru" => "Перенесите файл сюда",
        "uk" => "Перенесіть файл сюди"
    ),
    "ppass" => array(
        "en" => "Password",
        "ru" => "Пароль",
        "uk" => "Пароль"
    ),
    "pass" => array(
        "en" => "password",
        "ru" => "пароль",
        "uk" => "пароль"
    ),
    "decodedData" => array(
        "en" => "Decoded data",
        "ru" => "Декодированные данные",
        "uk" => "Декодовані дані"
    ),
    "notFound" => array(
        "en" => "data not found",
        "ru" => "данные не найдены",
        "uk" => "дані не знайдені"
    ),
    "copy" => array(
        "en" => "copy",
        "ru" => "копировать",
        "uk" => "копіювати"
    ),
    "download" => array(
        "en" => "download",
        "ru" => "скачать",
        "uk" => "завантажити"
    )
);
?>


<script src="/libraries/seedRandom/seedrandom.js" async></script>
<link href="/libraries/dropZone/dropZone.css" rel="stylesheet">
<script src="/libraries/dropZone/dropZone.js"></script>

<script src="/blocks/content/script.min.js"></script>
<link href="/blocks/content/style.min.css" rel="stylesheet">

<div class="content-container flex f-column f-vCenter">

    <div class="flex nr">
        <div
             class="radiobutton button-easy-blue button-left js-button-encoding"
             data-radio-button-function-name="showEncodingMenu"
        >
            <?=$phrase["encoding"][$lang]; ?>
        </div>
        <div
             class="radiobutton button-easy-blue button-right js-button-decoding"
             data-radio-button-function-name="showDecodingMenu"
        >
            <?=$phrase["decoding"][$lang]; ?>
        </div>
    </div>


    <div class="w-100 content flex">
        <div class="w-100 flex f-column f-vhCenter">

            <!-- encoding -->


            <div class="w-100 f f-column encoding-container">
                <div class="f-hCenter encode-input-blocks">


                    <div class="flex f-column encod-left">
                        <div class="like-title-2">
                            <?=$phrase["image"][$lang]; ?>
                        </div>

                        <div
                            class="dropZone dropZone-en-im"
                            data-drop-zone-function-name="openFile">
                            <div class="dz-fileName-fake"></div>
                            <div class="dz-title">
                                <?=$phrase["dropImage"][$lang]; ?>
                            </div>
                            <div class="dz-fileName"><div></div></div>
                        </div>

                        <div class="img-info">
                            <div class="img-info-title">
                                <div class="img-info-title__space"></div>
                                <div class="img-info-title__full-size">
                                    <?=$phrase["pixels"][$lang]; ?>
                                </div>
                                <div class="img-info-title__x-size">
                                    <?=$phrase["size"][$lang]; ?>, px
                                </div>
                            </div>
                            <div class="img-info-content img-info__need">
                                <div>
                                    <div class="img-info-circle"></div>
                                </div>
                                <div class="img-info-title">
                                    <?=$phrase["need"][$lang]; ?>
                                    >=
                                </div>
                                <div class="img-info-vLine"></div>
                                <div class="img-info-full-size">
                                    —
                                </div>
                                <div class="img-info-vLine"></div>
                                <div class="img-info-x-size">
                                    — x —
                                </div>
                            </div>
                            <div class="img-info-content img-info__available">
                                <div>
                                    <div class="img-info-circle"></div>
                                </div>
                                <div class="img-info-title">
                                    <?=$phrase["inStock"][$lang]; ?>
                                    >=
                                </div>
                                <div class="img-info-vLine"></div>
                                <div class="img-info-full-size">
                                    —
                                </div>
                                <div class="img-info-vLine"></div>
                                <div class="img-info-x-size">
                                    — x —
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="flex f-column encod-center">
                        <div class="like-title-2">
                            <?=$phrase["data"][$lang]; ?>
                        </div>
                        <div class="flex f-space f-vhCenter en-cen-txt-or-fl-bt">
                            <div
                                 class="radiobutton radiobutton-active button-easy-blue f f-space f-vhCenter en-cen-txt-or-fl-txt">
                                <?=$phrase["text"][$lang]; ?>
                            </div>
                            <div class="radiobutton button-easy-blue f f-space f-vhCenter en-cen-txt-or-fl-fl">
                                <?=$phrase["file"][$lang]; ?>
                            </div>
                        </div>
                        <div class="f f-space en-cen-input-container">
                            <div class="f-space en-cen-input">
                                <textarea
                                    placeholder="<?=$phrase["txtForEnc"][$lang]; ?>"
                                    rows="5"
                                ></textarea>
                            </div>
                            <div
                                class="dropZone f-space dropZone-en-f"
                                data-drop-zone-function-name="openFile">
                                <div class="dz-fileName-fake"></div>
                                <div class="dz-title">
                                    <?=$phrase["dropFile"][$lang]; ?>
                                </div>
                                <div class="dz-fileName"><div></div></div>
                            </div>
                        </div>
                    </div>
                    <div class="flex f-column encod-right">
                        <div class="like-title-2">
                            <?=$phrase["ppass"][$lang]; ?>
                        </div>
                        <div class="encod-right__input">
                            <textarea
                                    placeholder="<?=$phrase["pass"][$lang]; ?>"
                                    rows="5"
                            ></textarea>
                        </div>
                    </div>
                </div>
                <div class="en-b-d-container f f-vhCenter">
                    <div class="en-b-d-container__fake-download"></div>
                    <div class="button-gray button-big f f-vhCenter encoding">
                        <?=$phrase["encode"][$lang]; ?>
                    </div>
                    <div class="button-gray encoding-download">
                        <img src="/images/arrow-bottom.svg" alt="↓">
                        <a class="encoding-download-link" href="#"></a>
                    </div>
                </div>
            </div>
            <!-- decoding -->
            <div class="f f-column decoding-container">
                <div class="w-100 f">
                    <div class="flex f-column">
                        <div class="like-title-2">
                            <?=$phrase["image"][$lang]; ?>
                        </div>
                        <div
                            class="dropZone dropZone-de-im"
                            data-drop-zone-function-name="openFile">
                            <div class="dz-fileName-fake"></div>
                            <div class="dz-title">
                                <?=$phrase["dropImage"][$lang]; ?>
                            </div>
                            <div class="dz-fileName"><div></div></div>
                        </div>
                    </div>
                    <div class="flex f-column">
                        <div class="like-title-2">
                            <?=$phrase["ppass"][$lang]; ?>
                        </div>
                        <div class="de-password-input">
                            <textarea
                                placeholder="<?=$phrase["pass"][$lang]; ?>"
                                rows="5"
                            ></textarea>
                        </div>
                    </div>
                </div>
                <div class="en-b-d-container f f-column f-vhCenter">
                    <div class="button-gray button-big f f-vhCenter decoding">
                        <?=$phrase["decode"][$lang]; ?>
                    </div>
                    <div class="decoding-output-container">
                        <div class="f f-hCenter w-100">
                            <?=$phrase["decodedData"][$lang]; ?>:
                        </div>
                        <div class="decoding-output-data">
                            <div class="f f-hCenter decoding-output-data__null">
                                <?=$phrase["notFound"][$lang]; ?>
                            </div>
                            <div class="f f-column f-vCenter decoding-output-data__text">
                                <div class="decoding-output-data-text-block">
                                    <textarea></textarea>
                                </div>
                                <div class="button-easy-blue w-100 decoding-output-data-text-copy-button">
                                    <?=$phrase["copy"][$lang]; ?>
                                </div>
                            </div>
                            <div class="f f-vhCenter decoding-output-data__file">
                                <div class="decoding-output-data-file-name"></div>
                                <div class="button-easy-blue decoding-download">
                                    <?=$phrase["download"][$lang]; ?>
                                    <a class="decoding-download-link" href="#"></a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="divCanvas"></div>
</div>
