<script>
    let lang;
    let l;
    let url;
    let date;
    let boolCookieNull = 0;

    console.log(document.cookie.length);

    url = window.location.href;
    if(url.indexOf('lang=') === -1){
        lang = navigator.language || navigator.userLanguage;
        if(lang !== "en" && lang !== "uk" && lang !== "ru")
            lang = 'en';
    } else {
        l = url.indexOf('lang=');
        lang = url.substr(l+5, 2);
    }

    if(document.cookie.length > 0)
        boolCookieNull = 1;
    date = new Date(Date.now() );
    date.setDate(date.getDate() + 3);
    document.cookie = 'allie_language=' + lang + '; expires=' + date + '; SameSite=Lax;  path=/';

    console.log(lang);
    if(boolCookieNull === 0)
        location.reload();
</script>

<?php
    if(isset($_GET['lang'])){
        $lang = $_GET['lang'];
    } else if (isset($_COOKIE['allie_language'])){
        $lang = $_COOKIE['allie_language'];
    } else {
        $lang = 'en';
    }


function printInConsole($value){
    echo "<script>console.log('".$value."')</script>";
}

?>