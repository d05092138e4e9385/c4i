if( document.readyState !== 'loading' ) {
    bottomContainer();
} else {
    document.addEventListener("DOMContentLoaded", bottomContainer);
}
//
function bottomContainer(){
	let urlInfo;
	let timeoutId;

	initBottom();
    function initBottom(){
        document.querySelector('.bottom-iframe-container').addEventListener('click', hide);
		urlInfo = document.querySelector('.bottom-l1').href;
		
        setLinkSettings();
    }

    function setLinkSettings(){
		elem = document.querySelector('.bottom-l1');
        elem.addEventListener('click', clickLink);
        elem.href += '/?lang=' + lang;
		document.querySelector('.bottom-l2').href += '/?lang=' + lang;
		elem.style.display = 'flex';
    }
    function clickLink(event) {
        event.preventDefault();
        //urlInfo += '/index_mini.php/?lang=' + lang;
        showInfoBlock(document.querySelector('.bottom-l2').href);
    }
    function showInfoBlock(url){
        let elem = document.querySelector("#infoFrame");
        elem.addEventListener('load', function(){
            show();
        });
        elem.src = url;
    }

    function show(){
        timeoutId = undefined;
        let elemContainer = document.querySelector('.bottom-iframe-container');
        let elemIframe = document.querySelector('#infoFrame');

        if(elemIframe.classList.contains('displayHide')) {
            elemIframe.classList.remove('displayHide');
        }

        if(elemContainer.classList.contains('bottom-container-hide')) {
            elemContainer.classList.remove('bottom-container-hide');
        }
        if(!elemContainer.classList.contains('bottom-container-show')) {
            elemContainer.classList.add('bottom-container-show');
        }

        if(elemIframe.classList.contains('bottom-iframe-hide')) {
            elemIframe.classList.remove('bottom-iframe-hide');
        }
        if(!elemIframe.classList.contains('bottom-iframe-show')) {
            elemIframe.classList.add('bottom-iframe-show');
        }
    }
    function hide(){
        let elemContainer = document.querySelector('.bottom-iframe-container');
        let elemIframe = document.querySelector('#infoFrame');

        if(elemContainer.classList.contains('bottom-container-show')) {
            elemContainer.classList.remove('bottom-container-show');
        }
        if(!elemContainer.classList.contains('bottom-container-hide')) {
            elemContainer.classList.add('bottom-container-hide');
        }

        if(elemIframe.classList.contains('bottom-iframe-show')) {
            elemIframe.classList.remove('bottom-iframe-show');
        }
        if(!elemIframe.classList.contains('bottom-iframe-hide')) {
            elemIframe.classList.add('bottom-iframe-hide');
        }

        timeoutId = setTimeout(hideDisplay, 500);
    }
    function hideDisplay(){
        let elem = document.querySelector('.bottom-iframe-container');

        if(!elem.classList.contains('displayHide')) {
            elem.classList.add('displayHide');
        }
        if(elem.classList.contains('bottom-container-hide')) {
            elem.classList.remove('bottom-container-hide')
        }
    }

}