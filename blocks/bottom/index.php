<link href="/blocks/bottom/style.min.css" rel="stylesheet">
<script src="/blocks/bottom/script.min.js" async></script>
<div class="flex f-vhCenter bottom">
    <a class="bottom-l1" href="/pages/siteInfo">
        about this web-site
    </a>
	<a class="bottom-l2" href="/pages/siteInfo/index_mini.php"></a>
    <div class="flex f-hCenter bottom-iframe-container">
        <iframe id="infoFrame" sandbox="allow-same-origin"></iframe>
    </div>
</div>