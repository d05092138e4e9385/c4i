if( document.readyState !== 'loading' ) {
    menuTopInit();
} else {
    document.addEventListener("DOMContentLoaded", menuTopInit);
}

function menuTopInit(){
    document.querySelector('.js-en').addEventListener('click', function(){setLanguage('en');});
    document.querySelector('.js-ru').addEventListener('click', function(){setLanguage('ru');});
    document.querySelector('.js-ua').addEventListener('click', function(){setLanguage('uk');});
    document.querySelector('.menu-top__content').style.display = 'flex';
}
/*
function preSetFontSize(){
    if(screen.width > screen.height){
        document.querySelector('body').style.fontSize = (screen.availHeight / 50.2) + 'px';
    } else{
        document.querySelector('body').style.fontSize =
            ((screen.width < screen.availWidth ? screen.width : screen.availWidth) / 124.8) + 'px';
    }
}
*/
function setLanguage(newLang){
    if(newLang === lang) {return;}
    let url;
    let l;

    url = window.location.href;
    l = url.lastIndexOf('/');
    if(l === url.length - 1){
        url = url.substr(0, l);
    }
    if(url.substr(l, url.length - l).indexOf('lang=') !== -1){
        url = url.substr(0, l);
    }
    url = url + '?lang=' + newLang;
    window.location.replace(url);
}