<?php
if(!isset($lang))
    $lang = 'en';
$temp = array(
    "language" => array(
        "en" => "Language",
        "ru" => "Язык",
        "uk" => "Мова"
    )
);
if(isset($phrase))
    $phrase += $temp;
else
    $phrase = $temp;

//printInConsole($_COOKIE['allie_language']);

?>

<link href="/blocks/menu-top/style.min.css" rel="stylesheet">
<script src="/blocks/menu-top/script.min.js" async></script>

<div class="menu-top">
    <div class="menu-top__content" style="display: none;">

        <a class="nr button-easy-gray menu-top__button-home" href="/?lang=<?=$lang; ?>">
            HOME
        </a>

        <div class="flex-space"></div>

        <div class="flex menu-language">
            <div class="menu-top__language-name flex f-vCenter">
                <?=$phrase["language"][$lang]; ?>:
            </div>
            <div class="js-en icon-lang <?= ($lang == 'en') ? 'langActive' : 'langInert'; ?>">
                <svg
                        xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 30" width="1200" height="600">
                    <clipPath id="t">
                        <path d="M30,15 h30 v15 z v15 h-30 z h-30 v-15 z v-15 h30 z"/>
                    </clipPath>
                    <path d="M0,0 v30 h60 v-30 z" fill="#00247d"/>
                    <path d="M0,0 L60,30 M60,0 L0,30" stroke="#fff" stroke-width="6"/>
                    <path d="M0,0 L60,30 M60,0 L0,30" clip-path="url(#t)" stroke="#cf142b" stroke-width="4"/>
                    <path d="M30,0 v30 M0,15 h60" stroke="#fff" stroke-width="10"/>
                    <path d="M30,0 v30 M0,15 h60" stroke="#cf142b" stroke-width="6"/>
                </svg>
            </div>
            <div class="js-ru icon-lang <?= ($lang == 'ru') ? 'langActive' : 'langInert'; ?>">
                <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="400" height="200" viewBox="0, 0, 400,200">
                    <path d="M0.000 33.400 L 0.000 66.800 200.000 66.800 L 400.000 66.800 400.000 33.400 L
    	           		400.000 0.000 200.000 0.000 L 0.000 0.000 0.000 33.400 " stroke="none" fill="#fcfcfc"
                          fill-rule="evenodd"></path>
                    <path d="M0.000 166.600 L 0.000 200.000 200.000 200.000 L 400.000 200.000 400.000 166.600
    	           		L 400.000 133.200 200.000 133.200 L 0.000 133.200 0.000 166.600 " stroke="none"
                          fill="#fc0404" fill-rule="evenodd"></path>
                    <path d="M0.000 100.000 L 0.000 133.200 200.000 133.200 L 400.000 133.200 400.000 100.000
    	           		L 400.000 66.800 200.000 66.800 L 0.000 66.800 0.000 100.000 " stroke="none"
                          fill="#0404fc" fill-rule="evenodd"></path>
                </svg>
            </div>
            <div class="js-ua icon-lang <?= ($lang == 'uk') ? 'langActive' : 'langInert'; ?>">
                <svg
                        xmlns="http://www.w3.org/2000/svg" width="400" height="200" viewBox="0, 0, 400,200">
                    <path d="M0.000 150.000 L 0.000 200.000 200.000 200.000 L 400.000 200.000 400.000 150.000
    	        		L 400.000 100.000 200.000 100.000 L 0.000 100.000 0.000 150.000 " stroke="none"
                          fill="#fcfc04" fill-rule="evenodd"></path>
                    <path d="M0.000 50.000 L 0.000 100.000 200.000 100.000 L 400.000 100.000 400.000 50.000 L
    	        		400.000 0.000 200.000 0.000 L 0.000 0.000 0.000 50.000 " stroke="none" fill="#0404fc"
                          fill-rule="evenodd"></path>
                </svg>
            </div>
        </div>

    </div>
</div>