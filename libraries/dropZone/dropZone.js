document.addEventListener("DOMContentLoaded", dropZoneInit);

function dropZoneInit(){
    let elems = document.querySelectorAll('.dropZone');

    for(let i = 0; i < elems.length; i++){
        if(elems[i].dataset.dropZoneFunctionName) {
            createDropZoneElement(elems[i], elems[i].dataset.dropZoneFunctionName);
        }
    }
}
function createDropZoneElement(element, funcName){
    let elemInput;

    if(!element.dataset.dropZoneFunctionName)
        element.dataset.dropZoneFunctionName = funcName;

    elemInput = document.createElement('input');
    elemInput.type = 'file';
    elemInput.classList.add('dropZone-input');

    element.addEventListener('click', function(){elemInput.click();});
    element.addEventListener('dragover', dropZoneHandleDragOver);
    element.addEventListener('drop', dropZoneHandleFileSelect);
    elemInput.addEventListener('change', function(){window[funcName](this.files, this.parentElement)});
    element.append(elemInput);
}

function dropZoneHandleDragOver(e) {
    e.stopPropagation();
    e.preventDefault();
    e.dataTransfer.dropEffect = 'copy';
}
function dropZoneHandleFileSelect(e) {
    e.stopPropagation();
    e.preventDefault();

    let files = e.dataTransfer.files;
    let funcName = e.target.dataset.dropZoneFunctionName;
    window[funcName](files, e.target);
}
