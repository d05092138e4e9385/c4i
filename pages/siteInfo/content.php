<?php
if(!isset($lang))
    $lang = 'en';
$phrase	= array(
    "description1" => array(
        "uk" => "
            <p>
                Цей проект призначений для кодування і шифрування інформації у зображення (лише \"png\" та \"jpeg\"/\"jpg\" формати), непомітно змінюючи кольори пікселів. 
                Завдяки цьому можливо приховати наявність передачі певної інформації, при використанні незахищеного каналу зв'язку.
            </p><p>              
                Запис інформації в зображення відбувається наступним чином:
            <br>
                1. Інформація, призначена для запису в зображення, кодується в двійкову систему числення.
            <br>               
                2. До цієї інформації додається технічна інформація: розмір інформації, тип даних (текст чи файл).
            <br>
                3. Зображення змінюється на основі попередньо створеної бінарної інформації. 
                Береться перший піксель зображення і перші три біти інформації. 
                Червоний, зелений та блакитний кольори пікселю змінюються відповідно до цих трьох бітів: якщо біт = 0 — значення кольору стає парним (без остачі ділиться на 2),
                    за потреби збільшуючись на 1. Якщо біт = 1 — значення кольору стає не парним.
                Після цього береться наступний піксель і наступні три біти інформації. Так триває доки вся інформація не стане закодованою в зображення.
                При використанні паролю, під час кодування інформації в зображення, порядок запису інформації в пікселі буде змінено на основі цього паролю.
            </p><p>
                В цьому проекті використано наступні сторонні бібліотеки:
            </p>
            ",
        "ru" => "
            <p>
                Этот проект предназначен для кодирования и шифрования информации в изображения (только \"png\" и \"jpeg\" / \"jpg\" форматы), незаметно меняя цвета пикселей. 
             Благодаря этому возможно скрыть наличие передачи определенной информации, при использовании незащищенного канала связи.
            </p><p>
                Запись информации в изображения происходит следующим образом:
            <br>
                1. Информация, предназначенная для записи в изображения, кодируется в двоичную систему счисления.
            <br>
                2. К ней прилагается техническая информация: размер информации, тип данных (текст или файл).
            <br>
                3. Изображение меняется на основе предварительно созданной бинарной информации. 
                Берется первый пиксель изображения и первые три бита информации. 
                Красный, зеленый и голубой цвета пикселя изменяются в соответствии с этих трех битов: если бит = 0 - значение цвета становится парным 
                    (без остатка делится на 2), при необходимости увеличиваясь на 1. 
                Если бит = 1 - значение цвета становится непарные. После этого берется следующий пиксель и следующие три бита информации. 
                Так продолжается пока вся информация не станет закодированной в изображение. При использовании пароля при кодировании информации в изображение, 
                    порядок записи информации в пиксели будут изменены на основе этого пароля.
            </p><p>   
                В этом проекте использованы следующие сторонние библиотеки:
            </p>
                ",
        "en" => "
                This project is intended to encode and encrypt information into images (only \"png\" and \"jpeg\" / \"jpg\" formats), without changing the colors of pixels. 
                This makes it possible to conceal the transmission of certain information when using an unsecured communication channel.
            </p><p>
                The information is recorded in the image as follows:
            <br>
                1. The information to be written to the image is encoded into a binary number system.
            <br>
                2. Technical information is added to this information: information size, data type (text or file).
            <br>
                3. The image changes based on previously created binary information. 
                The first image pixel and the first three bits of information are taken. 
                The red, green and blue colors of the pixel change according to these three bits: if bit = 0 - the color value becomes even 
                (without dividing by 2), if necessary increasing by 1. If bit = 1 - the color value becomes not even.
                 The next pixel and the next three bits of information are then taken. This continues until all the information is encoded into the image. 
                 When using a password when encoding information into an image, the order of recording information in a pixel will be changed based on that password.
            </p><p>   
                The following third-party libraries are used in this project:
            "
    ),
    "description2" => array(
        "en" => " — to create a pseudo-randomness based on seed.",
        "ru" => " — для создания псевдо случайности на основе зерна.",
        "uk" => " — для створення псевдо випадковості на основі зерна."
    )
);
?>

<div class="content-container">
    <?=$phrase["description1"][$lang]; ?>
    <p>
        <a href="https://github.com/davidbau/seedrandom" target="_blank">
            seedRandom
        </a>
        <?=$phrase["description2"][$lang]; ?>
    </p>
</div>