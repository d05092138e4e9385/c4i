﻿<?php
    $lang = 'en';
    include $_SERVER['DOCUMENT_ROOT'].'/blocks/takeLanguage.php';
?>
<!DOCTYPE html>
<html lang="<?php $lang?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>C4I Information</title>
	<link href="/styles/main.min.css" rel="stylesheet">
    <link href="/styles/style.min.css" rel="stylesheet">
    <link href="/pages/siteInfo/style.css" rel="stylesheet">
</head>
<body>
    <?php include $_SERVER['DOCUMENT_ROOT'].'/blocks/menu-top/index.php'; ?>
    <?php include 'content.php'; ?>
</body>
</html>